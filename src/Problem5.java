import java.util.Scanner;

import javax.sql.rowset.spi.SyncResolver;

public class Problem5 {
    public static void main(String[] args) {
        String str;
        Scanner sc = new Scanner(System.in);
        while(true){
            System.out.print("please input: ");
            str = sc.nextLine();   
            if(str.equalsIgnoreCase("bye")){
                System.out.println("Exit Program");
                System.exit(0); 
            }
            System.out.println(str);
        }   
    }
}
