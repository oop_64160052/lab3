import java.util.Scanner;

public class Problem9While {
    public static void main(String[] args) {
        int number;
        Scanner sc = new Scanner(System.in);
        System.out.print("Please input n: ");
        number = sc.nextInt();
        int i = 1;
        while(i <= number){
            int j = 1;
            while(j <= number){
                System.out.print(j);
                j++;
            }
            System.out.println();
            i++;
        }
    }
}
