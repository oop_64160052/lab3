import java.util.Scanner;

public class Problem18 {
    public static void main(String[] args) {
        int type;
        Scanner sc = new Scanner(System.in);
        System.out.print("Please select star type [1-4,5 is Exit]: ");
        type = sc.nextInt();
        while(true){
            if(type > 5){
                System.out.println("Error: please input number between 1-5");
                break;
            }else if(type == 5){
                System.out.println("Bye bye!!!");
                break;
            }else{
                int num;
                Scanner sc2 = new Scanner(System.in);
                System.out.print("Please input number: ");
                num = sc2.nextInt();
                if(type == 1){
                    for(int i = 1; i <= num; i++){
                        for(int j = 0; j < i; j++){
                            System.out.print("*");
                        }
                        System.out.println();
                    }
                    break;
                }else if(type == 2){
                    for(int i= num; i>=1; i--){
                        for(int j = 0; j < i; j++){
                            System.out.print("*");   
                        }
                        System.out.println();
                }
                break;
            
                }else if (type == 3){
                    for(int i = 0; i < num; i++){
                        for(int j = 0; j < num; j++){
                            if(j >= i){
                                System.out.print("*");
                            }else{
                                System.out.print(" ");
                        }
                    }
                        System.out.println();
                }
                    break;
                }else if(type == 4) {
                    for(int i = num -1; i >= 0; i--){
                        for(int j = 0; j < num; j++){
                            if (j >= i){
                                System.out.print("*");
                            }else {
                                System.out.print(" ");
                        }
                    }
                        System.out.println();
                }
                    break;
            }
        }
    }
    }
}
